// 2r1w regiter file. Should be synthesised into a pair of M10K blocks.
// (10% efficiency for this size, however...)
module Regfile #(parameter data_width = 32,
					  parameter num_regs = 32,
					  parameter select_width = $clog2(num_regs))
					 (input logic clk,
					  input logic [select_width-1:0] a_select,
					  input logic [select_width-1:0] b_select,
					  input logic [select_width-1:0] write_a_select,
					  input logic [select_width-1:0] write_b_select,
					  input logic write_a_enable,
					  input logic write_b_enable,
					  input logic [data_width - 1 : 0] write_a_data,
					  input logic [data_width - 1 : 0] write_b_data,
					  output logic [data_width - 1 : 0] a,
					  output logic [data_width - 1 : 0] b);
	logic [data_width - 1 : 0] data [num_regs - 1 : 0];
	always @(posedge clk) begin
		if (write_a_enable) begin
			data[write_a_select] <= write_a_data;
		end
		if (write_b_enable) begin
			// Give write_a priority.
			if (!write_a_enable || write_a_select != write_b_select) begin
				data[write_b_select] <= write_b_data;
			end
		end
		if (!write_a_enable && !write_b_enable) begin
			a <= data[a_select];
			b <= data[b_select];
		end
	end
endmodule
