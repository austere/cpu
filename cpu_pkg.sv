package cpu_pkg;
	typedef enum logic [2:0] {
		kStateFetch = 3'd0,
		kStateDecode = 3'd1,
		kStateExecute = 3'd2,
		kStateMemoryAccess = 3'd3,
		kStateWriteBack = 3'd4,
		kStateHalted = 3'd7
	} cpu_state_t;

	// CPU configuration.
	parameter data_width = 32;
	parameter address_width = 32;
	// Number of each input and output FIFO interface.
	parameter num_fifos = 8;
	
	localparam reg_width = 5;
	localparam opcode_width = 5;
	localparam funct_width = 4;
	// 32 link registers including Link Register (LR) and Stack Pointer (SP).
	localparam num_regs = 1 << reg_width;
	localparam link_register = num_regs - 1;
	localparam stack_pointer = num_regs - 2;
	localparam instruction_width = data_width;
	// Width of the internal memory for instruction and data. This defines
	// the depth of the memory as well.
	localparam imem_address_width = 8;
	localparam imem_depth = 1 << imem_address_width;

	function logic in_imem(logic [address_width - 1 : 0] address_out);
		localparam imem_mask = imem_depth - 1;
		return (address_out & ~imem_mask) == '0;
	endfunction

	// Opcodes.
	typedef enum logic [4:0] {
		kOpcodeSpecial = 5'd0,
		kOpcodeLoad = 5'd1,
		kOpcodeStore = 5'd2,
		kOpcodeMove = 5'd3,
		kOpcodeAlu = 5'd4,
		kOpcodeShift = 5'd5,
		kOpcodeCompare = 5'd7,
		kOpcodeBranch = 5'd8,
		kOpcodeBranchCond = 5'd9,
		kOpcodeFifo = 5'd10,
		//kOpcodeAluExt (multiply, divide)
		// ...
		kOpcodeHalt = 5'd31
	} opcode_t;

	// Funct values.
	localparam [funct_width - 1 : 0] kSpecialNop = 0;
	localparam [funct_width - 1 : 0] kSpecialClearFlags = 1;
	localparam [funct_width - 1 : 0] kSpecialHalt = 2;

	localparam [funct_width - 1 : 0] kLoadImmediate = 0;
	localparam [funct_width - 1 : 0] kLoadImmediateUpper = 1;
	localparam [funct_width - 1 : 0] kLoadRelative = 2;
	localparam [funct_width - 1 : 0] kLoadIndirect = 3;
	localparam [funct_width - 1 : 0] kLoadDisplacement = 4;
	localparam [funct_width - 1 : 0] kLoadIndirectPostIncrement = 5;
	localparam [funct_width - 1 : 0] kLoadIndirectPreDecrement = 5;

	localparam [funct_width - 1 : 0] kStoreRelative = 0;
	localparam [funct_width - 1 : 0] kStoreIndirect = 1;
	localparam [funct_width - 1 : 0] kStoreIndirectPostIncrement = 5;
	localparam [funct_width - 1 : 0] kStoreIndirectPreDecrement = 5;

	localparam [funct_width - 1 : 0] kMoveDirect = 0;
	localparam [funct_width - 1 : 0] kMoveNegate = 1;
	localparam [funct_width - 1 : 0] kMoveFlags = 2;
	localparam [funct_width - 1 : 0] kMoveZero = 3;
	
	localparam [funct_width - 1 : 0] kAluAdd = 0;
	localparam [funct_width - 1 : 0] kAluAdc = 1;
	localparam [funct_width - 1 : 0] kAluSub = 2;
	localparam [funct_width - 1 : 0] kAluSbb = 3;
	localparam [funct_width - 1 : 0] kAluAnd = 4;
	localparam [funct_width - 1 : 0] kAluOr = 5;
	localparam [funct_width - 1 : 0] kAluXor = 6;
	localparam [funct_width - 1 : 0] kAluBitc = 7;
	localparam [funct_width - 1 : 0] kAluAddi = 8;
	localparam [funct_width - 1 : 0] kAluAdci = 9;
	localparam [funct_width - 1 : 0] kAluSubi = 10;
	localparam [funct_width - 1 : 0] kAluSbbi = 11;
	localparam [funct_width - 1 : 0] kAluAndi = 12;
	localparam [funct_width - 1 : 0] kAluOri = 13;
	localparam [funct_width - 1 : 0] kAluXori = 14;
	localparam [funct_width - 1 : 0] kAluBitci = 15;
	
	localparam [funct_width - 1 : 0] kShiftLeft1 = 0;
	localparam [funct_width - 1 : 0] kShiftRight1 = 1;
	localparam [funct_width - 1 : 0] kShiftLeft2 = 2;
	localparam [funct_width - 1 : 0] kShiftRight2 = 3;
	localparam [funct_width - 1 : 0] kShiftLeft4 = 4;
	localparam [funct_width - 1 : 0] kShiftRight4 = 5;
	localparam [funct_width - 1 : 0] kShiftLeft8 = 6;
	localparam [funct_width - 1 : 0] kShiftRight8 = 7;
	localparam [funct_width - 1 : 0] kShiftArithRight = 9;
	localparam [funct_width - 1 : 0] kShiftRollLeft = 10;
	localparam [funct_width - 1 : 0] kShiftRollRight = 11;
	
	localparam [funct_width - 1 : 0] kCompareImmediate = 0;
	localparam [funct_width - 1 : 0] kCompareDirect = 1;
	localparam [funct_width - 1 : 0] kCompareTestImmediate = 2;
	localparam [funct_width - 1 : 0] kCompareTestDirect = 3;
	
	localparam [funct_width - 1 : 0] kBranchRelative = 0;
	localparam [funct_width - 1 : 0] kBranchAbsolute = 1;
	localparam [funct_width - 1 : 0] kBranchIndirect = 2;
	localparam [funct_width - 1 : 0] kBranchLookup = 3;
	localparam [funct_width - 1 : 0] kBranchRelativeLink = 8;
	localparam [funct_width - 1 : 0] kBranchAbsoluteLink = 9;
	localparam [funct_width - 1 : 0] kBranchIndirectLink = 10;
	localparam [funct_width - 1 : 0] kBranchLookupLink = 11;
	
	localparam [funct_width - 1 : 0] kBranchCondZero = 0;
	localparam [funct_width - 1 : 0] kBranchCondNotZero = 1;
	localparam [funct_width - 1 : 0] kBranchCondCarry = 2;
	localparam [funct_width - 1 : 0] kBranchCondNotCarry = 3;
	localparam [funct_width - 1 : 0] kBranchCondMinus = 4;
	localparam [funct_width - 1 : 0] kBranchCondPlus = 5;
	localparam [funct_width - 1 : 0] kBranchCondOverflow = 6;
	localparam [funct_width - 1 : 0] kBranchCondNotOverflow = 7;
	localparam [funct_width - 1 : 0] kBranchCondHigher = 8;
	localparam [funct_width - 1 : 0] kBranchCondLower = 9;
	localparam [funct_width - 1 : 0] kBranchCondGreaterEqual = 10;
	localparam [funct_width - 1 : 0] kBranchCondLessThan = 11;
	localparam [funct_width - 1 : 0] kBranchCondGreaterThan = 12;
	localparam [funct_width - 1 : 0] kBranchCondLessEqual = 13;
	
	localparam [funct_width - 1 : 0] kFifoPush = 0;
	localparam [funct_width - 1 : 0] kFifoPop = 1;
	localparam [funct_width - 1 : 0] kFifoPeek = 3;
	localparam [funct_width - 1 : 0] kFifoWaitReadyAll = 4;
	localparam [funct_width - 1 : 0] kFifoWaitValidAll = 5;
	localparam [funct_width - 1 : 0] kFifoWaitReadyAny = 6;
	localparam [funct_width - 1 : 0] kFifoWaitValidAny = 7;
	localparam [funct_width - 1 : 0] kFifoReadReady = 8;
	localparam [funct_width - 1 : 0] kFifoReadValid = 9;
	
	// Opcode encoders.
	
	function logic [instruction_width - 1 : 0] op_nop();
		return {kOpcodeSpecial, kSpecialNop, 23'd0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_clrf();
		return {kOpcodeSpecial, kSpecialClearFlags, 23'd0};
	endfunction

	function logic [instruction_width - 1 : 0] op_halt();
		return {kOpcodeSpecial, kSpecialHalt, 23'd0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_ldi(logic [reg_width - 1 : 0] r_dst, logic [15:0] immediate_16);
		return {kOpcodeLoad, kLoadImmediate, r_dst, 2'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_ldiu(logic [reg_width - 1 : 0] r_dst, logic [15:0] immediate_16);
		return {kOpcodeLoad, kLoadImmediateUpper, r_dst, 2'b0, immediate_16};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_ldr(logic [reg_width - 1 : 0] r_dst, logic [15:0] offset);
		return {kOpcodeLoad, kLoadRelative, r_dst, 2'b0, offset};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_ldm(logic [reg_width - 1 : 0] r_dst, logic [reg_width - 1 : 0] r_address);
		return {kOpcodeLoad, kLoadIndirect, r_dst, r_address, 13'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_ldd(logic [reg_width - 1 : 0] r_dst, logic [reg_width - 1 : 0] r_address, logic [7:0] offset);
		return {kOpcodeLoad, kLoadDisplacement, r_dst, r_address, 5'b0, offset};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_ldmp(logic [reg_width - 1 : 0] r_dst, logic [reg_width - 1 : 0] r_address);
		return {kOpcodeLoad, kLoadIndirectPostIncrement, r_dst, r_address, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_ldmm(logic [reg_width - 1 : 0] r_dst, logic [reg_width - 1 : 0] r_address);
		return {kOpcodeLoad, kLoadIndirectPreDecrement, r_dst, r_address, 13'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_str(logic [8:0] offset, logic [reg_width - 1 : 0] r_src);
		return {kOpcodeStore, kStoreRelative, 5'b0, r_src, 5'b0, offset};
	endfunction

	function logic [instruction_width - 1 : 0] op_stm(logic [reg_width - 1 : 0] r_address, logic [reg_width - 1 : 0] r_src);
		return {kOpcodeStore, kStoreIndirect, 5'b0, r_src, r_address, 8'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_stmp(logic [reg_width - 1 : 0] r_address, logic [reg_width - 1 : 0] r_src);
		return {kOpcodeStore, kStoreIndirectPostIncrement, 5'b0, r_src, r_address, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_stmm(logic [reg_width - 1 : 0] r_address, logic [reg_width - 1 : 0] r_src);
		return {kOpcodeStore, kStoreIndirectPreDecrement, 5'b0, r_src, r_address, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_mov(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] src);
		return {kOpcodeMove, kMoveDirect, dst, src, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_movn(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] src);
		return {kOpcodeMove, kMoveNegate, dst, src, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_movf(logic [reg_width - 1 : 0] dst);
		return {kOpcodeMove, kMoveNegate, dst, 18'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_movz(logic [reg_width - 1 : 0] dst);
		return {kOpcodeMove, kMoveZero, dst, 18'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_add(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluAdd, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_adc(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluAdc, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_sub(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluSub, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_sbb(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluSbb, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_and(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluAnd, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_or(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluOr, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_xor(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluXor, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_bitc(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeAlu, kAluBitc, dst, r_a, r_b, 8'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_addi(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluAddi, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_adci(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluAdci, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_subi(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluSubi, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_sbbi(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluSbbi, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_andi(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluAndi, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_ori(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluOri, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_xori(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluXori, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_bitci(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeAlu, kAluBitci, dst, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_shl1(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftLeft1, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_shl2(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftLeft2, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_shl4(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftLeft4, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_shl8(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftLeft8, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_shr1(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftRight1, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_shr2(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftRight2, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_shr4(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftRight4, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_shr8(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftRight8, dst, r_a, 13'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_asr(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftArithRight, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_rol(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftRollLeft, dst, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_ror(logic [reg_width - 1 : 0] dst, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeShift, kShiftRollRight, dst, r_a, 13'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_cmp(logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeCompare, kCompareDirect, 5'b0, r_a, r_b, 8'b0};
	endfunction	

	function logic [instruction_width - 1 : 0] op_tst(logic [reg_width - 1 : 0] r_a, logic [reg_width - 1 : 0] r_b);
		return {kOpcodeCompare, kCompareTestDirect, 5'b0, r_a, r_b, 8'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_cmpi(logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeCompare, kCompareImmediate, 5'b0, r_a, 5'b0, immediate_8};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_tsti(logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeCompare, kCompareTestImmediate, 5'b0, r_a, 5'b0, immediate_8};
	endfunction

	function logic [instruction_width - 1 : 0] op_b(logic [15:0] immediate_16);
		return {kOpcodeBranch, kBranchRelative, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_ba(logic [15:0] immediate_16);
		return {kOpcodeBranch, kBranchAbsolute, 7'b0, immediate_16};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_bi(logic [reg_width - 1 : 0] r_a);
		return {kOpcodeBranch, kBranchIndirect, 5'b0, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_bm(logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeBranch, kBranchLookup, 5'b0, r_a, 5'b0, immediate_8};
	endfunction

	function logic [instruction_width - 1 : 0] op_bl(logic [15:0] immediate_16);
		return {kOpcodeBranch, kBranchRelativeLink, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bal(logic [15:0] immediate_16);
		return {kOpcodeBranch, kBranchIndirectLink, 7'b0, immediate_16};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_bil(logic [reg_width - 1 : 0] r_a);
		return {kOpcodeBranch, kBranchAbsoluteLink, 5'b0, r_a, 13'b0};
	endfunction

	function logic [instruction_width - 1 : 0] op_bml(logic [reg_width - 1 : 0] r_a, logic [7 : 0] immediate_8);
		return {kOpcodeBranch, kBranchLookupLink, 5'b0, r_a, 5'b0, immediate_8};
	endfunction

	function logic [instruction_width - 1 : 0] op_bz(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondZero, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bnz(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondNotZero, 7'b0, immediate_16};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_bcs(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondCarry, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bcc(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondNotCarry, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bmi(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondMinus, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bpl(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondPlus, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bvs(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondOverflow, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bvc(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondNotOverflow, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bhi(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondHigher, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bls(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondLower, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bge(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondGreaterEqual, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_blt(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondLessThan, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_bgt(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondGreaterThan, 7'b0, immediate_16};
	endfunction

	function logic [instruction_width - 1 : 0] op_ble(logic [15:0] immediate_16);
		return {kOpcodeBranchCond, kBranchCondLessEqual, 7'b0, immediate_16};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_pushf(logic [2:0] fifo_selector, logic [reg_width - 1 : 0] r_a);
		return {kOpcodeFifo, kFifoPush, 5'b0, r_a, 10'b0 , fifo_selector};
	endfunction

	function logic [instruction_width - 1 : 0] op_popf(logic [reg_width - 1 : 0] r_dst, logic [2:0] fifo_selector);
		return {kOpcodeFifo, kFifoPop, r_dst, 5'b0, 10'b0, fifo_selector};
	endfunction

	function logic [instruction_width - 1 : 0] op_peek(logic [reg_width - 1 : 0] r_dst, logic [2:0] fifo_selector);
		return {kOpcodeFifo, kFifoPop, r_dst, 5'b0, 10'b0, fifo_selector};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_wrall(logic [7 : 0] fifo_mask);
		return {kOpcodeFifo, kFifoWaitReadyAll, 15'b0, fifo_mask};
	endfunction

	function logic [instruction_width - 1 : 0] op_wvall(logic [7 : 0] fifo_mask);
		return {kOpcodeFifo, kFifoWaitValidAll, 15'b0, fifo_mask};
	endfunction

	function logic [instruction_width - 1 : 0] op_wrany(logic [7 : 0] fifo_mask);
		return {kOpcodeFifo, kFifoWaitReadyAny, 15'b0, fifo_mask};
	endfunction

	function logic [instruction_width - 1 : 0] op_wvany(logic [7 : 0] fifo_mask);
		return {kOpcodeFifo, kFifoWaitValidAny, 15'b0, fifo_mask};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_rrdy(logic [reg_width - 1 : 0] r_dst);
		return {kOpcodeFifo, kFifoReadReady, r_dst, 18'b0};
	endfunction
	
	function logic [instruction_width - 1 : 0] op_rvld(logic [reg_width - 1 : 0] r_dst);
		return {kOpcodeFifo, kFifoReadValid, r_dst, 18'b0};
	endfunction
endpackage
