module data_bus_responder #(parameter [7:0] prefix)
								 (input logic clk,
								  input logic [31:0] address, 
								  input logic [31:0] data_in,
								  input logic write_en,
								  input logic read_en,
								  output logic [31:0] data_out,
								  output logic data_acknowledge);
	logic [31:0] data [255 : 0];
	always @(posedge clk) begin
		if (read_en && address[31:24] == prefix) begin
			data_out <= data[address[7:0]];
			data_acknowledge <= 1;
		end else if (write_en && address[31:24] == prefix) begin
			data[address[7:0]] <= data_in;
			data_acknowledge <= 1;
		end else begin
			data_acknowledge <= 0;
		end
	end
endmodule

module cpu_tb();
	timeunit 1ns;
	timeprecision 10ps;

	import cpu_pkg::*;
	
	// Clock/reset block (50 MHz).
	logic clk;
	logic reset;
	initial clk = 0;
	always #10 clk = ~clk;

	// Test values.
	logic [31:0] reg_expected[logic [4:0]];
	logic [31:0] mem_expected[logic [7:0]];
	logic [31:0] external_expected[logic [7:0]];
	
	// Module connectivity.
	logic [31:0] address;
	logic [31:0] cpu_data_in;
	logic [31:0] cpu_data_out;
	logic [31:0] cpu_read_en;
	logic [31:0] cpu_write_en;
	logic [31:0] cpu_data_acknowledge;
	logic [31:0] cpu_address;
	
	logic [31:0] cpu_fifo_data_in;
	logic [31:0] cpu_fifo_data_out;
	logic [7:0] cpu_fifo_ready_in;
	logic cpu_fifo_ready_out;
	logic [7:0] cpu_fifo_valid_in;
	logic cpu_fifo_valid_out;
	logic [7:0] cpu_fifo_select;

	wire [31:0] fifo_data_out;
	wire fifo_ready_out;
	wire fifo_valid_out;
	wire fifo_select;
	
	assign fifo_select = cpu_fifo_select[0];
	assign cpu_fifo_valid_in = {7'b0, fifo_valid_out};
	assign cpu_fifo_ready_in = {7'b0, fifo_ready_out};
	assign cpu_fifo_data_in = (fifo_select) ? fifo_data_out : 'z;
	
	Cpu cpu0(.clk(clk), 
				.reset(reset),
				.pause(0),
				.address(cpu_address),
				.data_in(cpu_data_in),
				.data_out(cpu_data_out),
				.read_en(cpu_read_en),
				.write_en(cpu_write_en),
				.data_acknowledge(cpu_data_acknowledge),
				.fifo_data_in(cpu_fifo_data_in),
				.fifo_valid_in(cpu_fifo_valid_in),
				.fifo_ready_in(cpu_fifo_ready_in),
				.fifo_data_out(cpu_fifo_data_out),
				.fifo_valid_out(cpu_fifo_valid_out),
				.fifo_ready_out(cpu_fifo_ready_out),
				.fifo_select(cpu_fifo_select)
				);
				
	// External RAM responder
	data_bus_responder #(.prefix(8'h80)) responder0(
		.clk(clk),
		.address(cpu_address),
		.data_in(cpu_data_out),
		.data_out(cpu_data_in),
		.read_en(cpu_read_en),
		.write_en(cpu_write_en),
		.data_acknowledge(cpu_data_acknowledge));
	
	// FIFO responder
	fifo_grm #(.width(32), .depth_width(4)) fifo0(
					.clk(clk),
					.reset(reset),
					.data_in(cpu_fifo_data_out),
					.valid_in(cpu_fifo_valid_out & fifo_select),
					.ready_in(cpu_fifo_ready_out & fifo_select),
					.data_out(fifo_data_out),
					.valid_out(fifo_valid_out),
					.ready_out(fifo_ready_out));

	// Reset task (2 cycles).
	task reset_cpu();
		reset <= 1;
		#40 reset <= 0;
	endtask
	
	task init();
		reg_expected.delete();
		mem_expected.delete();
		external_expected.delete();
	endtask
	
	task fail();
		$display("FAILED");
		$stop;
	endtask
	
	task pass();
		$display("PASSED");
		$stop;
	endtask
	
	task check(string set_name);
		foreach(reg_expected[i]) begin
			if (reg_expected[i] !== cpu0.regs.data[i]) begin
				$display("Failed set:", set_name);
				$display("Expected R%d = %x, got R%d = %x", i, reg_expected[i], i, cpu0.regs.data[i]);
				fail();
			end
		end
		foreach(mem_expected[i]) begin
			if (mem_expected[i] !== cpu0.imem[i]) begin
				$display("Failed set:", set_name);
				$display("Expected imem[%d] = %x, got imem[%d] = %x", i, mem_expected[i], i, cpu0.imem[i]);
				fail();
			end
		end
		foreach(external_expected[i]) begin
			if (external_expected[i] !== responder0.data[i]) begin
				$display("Failed set:", set_name);
				$display("Expected imem[%d] = %x, got imem[%d] = %x", i, mem_expected[i], i, cpu0.imem[i]);
				fail();
			end
		end
		$display("Passed: %s", set_name);
	endtask
	
	// Compute the relative offset for a memory address.
	function logic [15:0] relative(int pc, int addr);
		return addr - pc - 1;
	endfunction
	
	task check_imem_loadstore();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.imem['h41] = 32'hc001d00d;

		// Instructions
		// ------------
		// LDI  R1, 0xbeef
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadImmediate, 5'd1, 2'b00, 16'hbeef};
		reg_expected[1] = 'hbeef;

		// LDI  R2, 0x40
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadImmediate, 5'd2, 2'b00, 16'h0040};
		reg_expected[2] = 'h40;

		// STM  [R2], R1
		cpu0.imem[pc++] = cpu_pkg::op_stm(2, 1);
		mem_expected['h40] = 'hbeef;

		// LDR  R3, [0x41]
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadRelative, 5'd3, 2'b00, relative(pc, 'h41)};
		reg_expected[3] = cpu0.imem['h41];

		// NOP
		cpu0.imem[pc++] = cpu_pkg::op_nop();

		// LDI  R4, 0x4321
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadImmediate, 5'd4, 2'b00, 16'h4321};
		// LDIU R4, 0x8765
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadImmediateUpper, 5'd4, 2'b00, 16'h8765};
		reg_expected[4] = 'h87654321;
		
		// LDI  R5, 0x30
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadImmediate, 5'd5, 2'b00, 16'h30};
		reg_expected[5] = 'h30;
		
		// LDD  R6, [R5, 0x10] (0x40) = 0xbeef
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadDisplacement, 5'd6, 5'd5, 5'd0, 8'h10};
		reg_expected[6] = 'hbeef;
		
		// HLT. Don't catch fire though.
		cpu0.imem[pc++] = {kOpcodeSpecial, kSpecialHalt, 23'd0};

		// [x] kSpecialNop
		// [x] kLoadImmediate
		// [x] kLoadImmediateUpper
		// [x] kLoadRelative
		// [ ] kLoadIndirect
		// [x] kLoadDisplacement
		// [ ] kStoreRelative
		// [x] kStoreIndirect
		
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("imem_loadstore");
	endtask
	
	task check_ext_loadstore();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = 32'h80000000;
		cpu0.regs.data[1] = 32'h80000001;
		cpu0.regs.data[2] = 32'h22446688;
		responder0.data[0] = 32'h11335577;

		// Instructions
		// ------------
		// LDM R3, [R0]
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadIndirect, 5'd3, 5'd0, 13'd0};
		reg_expected[3] = responder0.data[0];
		// STM [R1], R2
		cpu0.imem[pc++] = cpu_pkg::op_stm(1, 2);
		external_expected[1] = cpu0.regs.data[2];
		// HLT (Halt at the end).
		cpu0.imem[pc++] = {kOpcodeSpecial, kSpecialHalt, 23'd0};

		// [x] kLoadIndirect
		// [x] kStoreIndirect
		
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("ext_loadstore");
	endtask

	task check_move();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = 5;
		cpu0.regs.data[1] = 6;
		cpu0.regs.data[8] = 32'hff00ff00;
		//cpu0.regs.data[9] = 32'h00ff00ff;

		// Instructions
		// ------------
		// MOV  R2, R0
		cpu0.imem[pc++] = {kOpcodeMove, kMoveDirect, 5'd2, 5'b0, 13'h0};
		reg_expected[2] = 'h5;
		// MOVN R3, R1
		cpu0.imem[pc++] = {kOpcodeMove, kMoveNegate, 5'd3, 5'b1, 13'h0};
		reg_expected[3] = ~'h6;
		// MOVZ R3
		cpu0.imem[pc++] = {kOpcodeMove, kMoveZero, 5'd8, 18'h0};
		reg_expected[8] = '0;
		// HLT
		cpu0.imem[pc++] = {kOpcodeSpecial, kSpecialHalt, 23'd0};

		// [x] kMoveDirect
		// [x] kMoveNegate
		// [ ] kMoveFlags
		// [x] kMoveZero
		
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("move");
	endtask
	
	task check_alu();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = 32'h12345678;
		cpu0.regs.data[1] = 32'h87654321;
		cpu0.regs.data[2] = 32'h11111111;
		cpu0.regs.data[3] = 32'h88888888;
		cpu0.regs.data[16] = 32'h0;
		
		// Instructions
		// ------------
		// ADD R4, R0, R1
		cpu0.imem[pc++] = {kOpcodeAlu, kAluAdd, 5'd4, 5'd0, 5'd1, 8'd0};
		reg_expected[4] = cpu0.regs.data[0] + cpu0.regs.data[1];
		// ADD R5, R1, R3
		cpu0.imem[pc++] = {kOpcodeAlu, kAluAdd, 5'd5, 5'd1, 5'd3, 8'd0};
		reg_expected[5] = cpu0.regs.data[1] + cpu0.regs.data[3];
		// SUB R6, R1, R3
		cpu0.imem[pc++] = {kOpcodeAlu, kAluSub, 5'd6, 5'd1, 5'd3, 8'd0};
		reg_expected[6] = cpu0.regs.data[1] - cpu0.regs.data[3];
		// MOVF R16
		cpu0.imem[pc++] = {kOpcodeMove, kMoveFlags, 5'd16, 18'h0};
		reg_expected[16] = {28'b0, 4'b0100};
		
		// HLT
		cpu0.imem[pc++] = {kOpcodeSpecial, kSpecialHalt, 23'd0};

		// [x] kAluAdd
		// [x] kAluSub
		// [ ] kAluAdc
		// [ ] kAluSbb
		// [ ] kAluAnd
		// [ ] kAluOr
		// [ ] kAluXor
		// [ ] kAluBitc
		// [ ] kAluAddi
		// [ ] kAluSubi
		// [ ] kAluAdci
		// [ ] kAluSbbi
		// [ ] kAluAndi
		// [ ] kAluOri
		// [ ] kAluXori
		// [ ] kAluBitci
		// Also tests:
		// [x] kMoveFlags
		
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("alu");
	endtask
	
	task check_basic_branch();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----

		// Instructions
		// ------------
		// BA 0x0080
		cpu0.imem[pc++] = op_ba(16'h0080);
		// This should never run.
		// LDI R1, 0x6666
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadImmediate, 5'd1, 2'b00, 16'h6666};
		// HLT (shouldn't run).
		cpu0.imem[pc++] = {kOpcodeSpecial, kSpecialHalt, 23'd0};

		// IMEM starting from 0x0080.
		pc = 'h80;
		// LDI R1, 0x7777
		cpu0.imem[pc++] = {kOpcodeLoad, kLoadImmediate, 5'd1, 2'b00, 16'h7777};
		reg_expected[1] = 16'h7777;
		// HLT (Halt at the end).
		cpu0.imem[pc++] = {kOpcodeSpecial, kSpecialHalt, 23'd0};

		// [x] kBranchAbsolute
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("basic_branch");
	endtask
	
	task check_condition_branch();
		automatic int pc = 0;
		init();

		// Instructions
		// ------------
		// 0 LDI R0, 0x0
		cpu0.imem[pc++] = op_ldi(0, 0);
		// 1 LDI R1, 0x50
		cpu0.imem[pc++] = op_ldi(1, 16'h50);
		// 2 LDI R2, 0x30
		cpu0.imem[pc++] = op_ldi(2, 16'h30);
		// 3 ADD R0, R0, R1
		cpu0.imem[pc++] = op_add(0, 0, 1);
		// 4 SUBI R2, R2, 0x1
		cpu0.imem[pc++] = op_subi(2, 2, 1);
		// 5 BNZ -3
		cpu0.imem[pc++] = op_bnz(relative(pc, 3));
		// 6 HLT
		cpu0.imem[pc++] = op_halt();
		// R0 === 0x50 * 0x30
		reg_expected[0] = 16'h50 * 16'h30;
		// R2 === 0
		reg_expected[2] = 0;

		// [x] kBranchNotZero

		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("condition_branch");
	endtask
	
	task check_post_pre_load_store();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = 'h10;
		cpu0.regs.data[1] = 'h04;
		cpu0.regs.data[2] = 'h33221100;

		// Instructions
		// ------------
		// HLT
		cpu0.imem[pc++] = op_stmp(0, 2);
		cpu0.imem[pc++] = op_addi(2, 2, 'h10);
		cpu0.imem[pc++] = op_subi(1, 1, 1);
		cpu0.imem[pc++] = op_bnz(relative(pc, 0));
		cpu0.imem[pc++] = op_halt();
		mem_expected['h10] = cpu0.regs.data[2];
		mem_expected['h11] = cpu0.regs.data[2] + 'h10;
		mem_expected['h12] = cpu0.regs.data[2] + 'h20;
		mem_expected['h13] = cpu0.regs.data[2] + 'h30;

		// [ ] kLoadIndirectPostIncrement
		// [ ] kLoadIndirectPreDecrement
		// [x] kStoreIndirectPostIncrement
		// [ ] kStoreIndirectPreDecrement

		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("post_pre_load_store");
	endtask

	task check_fifo();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = 32'h12345678;

		// Instructions
		// ------------
		// PUSHF 0, R0
		cpu0.imem[pc++] = op_pushf(0, 0);
		// POPF R1, 0
		cpu0.imem[pc++] = op_popf(1,0);
		reg_expected[1] = cpu0.regs.data[0];
		// HLT (Halt at the end).
		cpu0.imem[pc++] = op_halt();

		// [x] kFifoPush
		// [x] kFifoPop
		// [ ] kFifoPeek
		// [ ] kFifoWaitReadyAll
		// [ ] kFifoWaitValidAll
		// [ ] kFifoWaitReadyAny
		// [ ] kFifoWaitValidAny
		// [ ] kFifoReadReady
		// [ ] kFifoReadValid

		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("fifo");
	endtask
	
	task check_branch_lt(int x, int y);
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = x;
		cpu0.regs.data[1] = y;

		// Instructions
		// ------------
		// 0 CMP R0, R1
		cpu0.imem[pc++] = op_cmp(0, 1);
		// 1 BLT 4
		cpu0.imem[pc++] = op_blt(relative(pc, 4));
		reg_expected[2] = (x < y) ? 1  : 0;
		// 2 LDI R2, #0
		cpu0.imem[pc++] = op_ldi(2, 0);
		// 3 HLT
		cpu0.imem[pc++] = op_halt();
		// 4 LDI R2, #1
		cpu0.imem[pc++] = op_ldi(2, 1);
		// 5 HLT
		cpu0.imem[pc++] = op_halt();

		
		// [x] kBranchCondLessThan
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check($sformatf("blt %d %d", x ,y));
	endtask
	
	task check_branch_le(int x, int y);
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = x;
		cpu0.regs.data[1] = y;

		// Instructions
		// ------------
		// 0 CMP R0, R1
		cpu0.imem[pc++] = op_cmp(0, 1);
		// 1 BLT 4
		cpu0.imem[pc++] = op_ble(relative(pc, 4));
		reg_expected[2] = (x <= y) ? 1  : 0;
		// 2 LDI R2, #0
		cpu0.imem[pc++] = op_ldi(2, 0);
		// 3 HLT
		cpu0.imem[pc++] = op_halt();
		// 4 LDI R2, #1
		cpu0.imem[pc++] = op_ldi(2, 1);
		// 5 HLT
		cpu0.imem[pc++] = op_halt();

		
		// [x] kBranchCond
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check($sformatf("ble %d %d", x ,y));
	endtask
	
	task check_branch_ge(int x, int y);
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = x;
		cpu0.regs.data[1] = y;

		// Instructions
		// ------------
		// 0 CMP R0, R1
		cpu0.imem[pc++] = op_cmp(0, 1);
		// 1 BLT 4
		cpu0.imem[pc++] = op_bge(relative(pc, 4));
		reg_expected[2] = (x >= y) ? 1  : 0;
		// 2 LDI R2, #0
		cpu0.imem[pc++] = op_ldi(2, 0);
		// 3 HLT
		cpu0.imem[pc++] = op_halt();
		// 4 LDI R2, #1
		cpu0.imem[pc++] = op_ldi(2, 1);
		// 5 HLT
		cpu0.imem[pc++] = op_halt();

		
		// [x] kBranchCond
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check($sformatf("bge %d %d", x ,y));
	endtask

	task check_branch_gt(int x, int y);
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = x;
		cpu0.regs.data[1] = y;

		// Instructions
		// ------------
		// 0 CMP R0, R1
		cpu0.imem[pc++] = op_cmp(0, 1);
		// 1 BLT 4
		cpu0.imem[pc++] = op_bgt(relative(pc, 4));
		reg_expected[2] = (x > y) ? 1  : 0;
		// 2 LDI R2, #0
		cpu0.imem[pc++] = op_ldi(2, 0);
		// 3 HLT
		cpu0.imem[pc++] = op_halt();
		// 4 LDI R2, #1
		cpu0.imem[pc++] = op_ldi(2, 1);
		// 5 HLT
		cpu0.imem[pc++] = op_halt();

		
		// [x] kBranchCond
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check($sformatf("bgt %d %d", x ,y));
	endtask

	task check_branch_ls(int unsigned x, int unsigned y);
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = x;
		cpu0.regs.data[1] = y;

		// Instructions
		// ------------
		// 0 CMP R0, R1
		cpu0.imem[pc++] = op_cmp(0, 1);
		// 1 BLT 4
		cpu0.imem[pc++] = op_bls(relative(pc, 4));
		reg_expected[2] = (x < y) ? 1  : 0;
		// 2 LDI R2, #0
		cpu0.imem[pc++] = op_ldi(2, 0);
		// 3 HLT
		cpu0.imem[pc++] = op_halt();
		// 4 LDI R2, #1
		cpu0.imem[pc++] = op_ldi(2, 1);
		// 5 HLT
		cpu0.imem[pc++] = op_halt();

		
		// [x] kBranchCond
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check($sformatf("bls %x %x", x ,y));
	endtask
	
	task check_branch_hi(int unsigned x, int unsigned y);
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----
		cpu0.regs.data[0] = x;
		cpu0.regs.data[1] = y;

		// Instructions
		// ------------
		// 0 CMP R0, R1
		cpu0.imem[pc++] = op_cmp(0, 1);
		// 1 BLT 4
		cpu0.imem[pc++] = op_bhi(relative(pc, 4));
		reg_expected[2] = (x > y) ? 1  : 0;
		// 2 LDI R2, #0
		cpu0.imem[pc++] = op_ldi(2, 0);
		// 3 HLT
		cpu0.imem[pc++] = op_halt();
		// 4 LDI R2, #1
		cpu0.imem[pc++] = op_ldi(2, 1);
		// 5 HLT
		cpu0.imem[pc++] = op_halt();

		
		// [x] kBranchCond
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check($sformatf("bhi %x > %x = %s", x, y, (x > y) ? "True" : "False"));
	endtask

	// Copy this to make new checks!
	task check_template();
		automatic int pc = 0;
		init();

		// Use hierarchical reference to backdoor write memory into the CPU.
		// Data.
		// -----

		// Instructions
		// ------------
		// ...
		// HLT (Halt at the end).
		cpu0.imem[pc++] = op_halt();

		// [ ] kXxxYyy
		
		// Execute first instruction
		reset_cpu();
		repeat (3) @(posedge clk);
		wait(cpu0.halted);
		check("template");
	endtask

	initial begin
		automatic int eq_x = 0;
		automatic int eq_y = 0;
		
		check_imem_loadstore();
		check_move();
		check_alu();
		check_ext_loadstore();
		check_basic_branch();
		check_condition_branch();
		check_post_pre_load_store();
		check_fifo();
		
		repeat (256) begin
			eq_x = $urandom_range(0, 3);
			eq_y = $urandom_range(0, 3);
			if ($urandom_range(0, 1) == 0) begin
				eq_x = -eq_x;
			end
			if ($urandom_range(0, 1) == 0) begin
				eq_y = -eq_y;
			end
			check_branch_lt($random(), $random());
			check_branch_le(eq_x, eq_y);
			check_branch_gt($random(), $random());
			check_branch_ge(eq_x, eq_y);
			check_branch_ls($urandom(), $urandom());
			check_branch_hi($urandom(), $urandom());
		end
		pass();
	end
endmodule
