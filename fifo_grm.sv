// GRM for a FWFT type FIFO.
module fifo_grm #(
					parameter width = 8,
					parameter depth_width = 4
				 ) (
					input logic clk,
					input logic reset,
					input logic [width-1:0] data_in,
					input logic valid_in,
					input logic ready_in,
					output logic [width-1:0] data_out,
					output logic valid_out,
					output logic ready_out
				 );
	localparam mem_size = 1 << depth_width;

	typedef logic [width-1:0] queue_of_data[$];
	queue_of_data mem;
	
	int _mem_stored;	// Debugging value.
	assign _mem_stored = mem.size();
	
	assign write = valid_in && ready_out;
	assign read = valid_out && ready_in;
	assign mem_write_bypass = write && (next_mem_size() == 1);
	
	function queue_of_data next_mem();
		if (read && write)
			return {mem[1:$], data_in};
		else if (write)
			return {mem, data_in};
		else if (read)
			return mem[1:$];
		return mem;
	endfunction
	
	function bit [width-1:0] next_mem_size();
		if (read && write)
			return mem.size();
		else if (write)
			return mem.size()+1;
		else if (read)
			return mem.size()-1;
		return mem.size();
	endfunction

	// Write block.
	always @(posedge clk) begin
		if (reset)
			mem <= {};
		else
			mem <= next_mem();
		assert (mem.size() <= mem_size) else $fatal("FIFO GRM Memory limit exceeded.");
	end
	
	// Read block.
	always @(posedge clk) begin
		if (reset) begin
			data_out <= 0;
		end else begin
			if (mem_write_bypass)
				data_out <= data_in;
			else if (read)
				data_out <= mem[1];
			else if (next_mem_size() != 0)
				data_out <= mem[0];
			else
				data_out <= 'x;
		end
	end
	
	// Valid output.
	always @(posedge clk) begin
		if (reset)
			valid_out <= 0;
		else
			valid_out <= next_mem_size() != 0;
	end
	
	// Ready output
	always @(posedge clk) begin
		if (reset)
			ready_out <= 1;
		else
			ready_out <= next_mem_size() != mem_size;
	end
endmodule
