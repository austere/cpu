module Cpu (// Global signals.
				 input logic clk,
				 input logic reset,
				 // Special signals.
				 input logic pause,
				 output logic halted,
				 // Data bus.
				 input logic [cpu_pkg::data_width - 1 : 0] data_in,
				 input logic data_acknowledge,
				 output logic [cpu_pkg::data_width - 1 : 0] data_out,
				 output logic [cpu_pkg::data_width - 1 : 0] address,
				 output logic read_en,
				 output logic write_en,
				 // FIFO interface.
				 input logic [cpu_pkg::data_width - 1 : 0] fifo_data_in,
				 input logic [cpu_pkg::num_fifos - 1 : 0] fifo_valid_in,
				 input logic [cpu_pkg::num_fifos - 1 : 0] fifo_ready_in,
				 output logic [cpu_pkg::data_width - 1 : 0] fifo_data_out,
				 output logic fifo_valid_out,
				 output logic fifo_ready_out,
				 output logic [cpu_pkg::num_fifos - 1 : 0] fifo_select);
	import cpu_pkg::*;

	// Internal memory.
	logic [instruction_width - 1 : 0] imem [imem_depth - 1 : 0];
	
	cpu_state_t state;
	
	logic [imem_address_width - 1 : 0] pc;
	logic [instruction_width - 1 : 0] instruction;
	
	// Value that is relative to the PC.
	logic [instruction_width - 1 : 0] relative_pc;
	logic [instruction_width - 1 : 0] relative_pc8;
	
	// Loaded during decode stage from selected registers.
	logic [data_width - 1 : 0] register_a;
	logic [data_width - 1 : 0] register_b;
	// Next value for register_a.
	logic [data_width - 1 : 0] reg_update_data;
	// Result register for ALU and memory access.
	logic [data_width - 1 : 0] result;
	// Enable to write to register, done during the execute state or the memory
	// access state. Gets committed during fetch. If doing during the execute
	// state then skip straight to fetch.
	logic reg_write_enable;
	// Updates register by using the second port of the regfile to write to it.
	// Basically this is the second register write port.
	logic reg_update_enable;
	// If set high then we evaluate all the flags using result, a and b.
	// carry is updated by the ALU instructions themselves, the rest are
	// done via update_flags()
	logic update_flags_enable;
	// This holds the sign of the two operators used in an alu operation.
	logic alu_sign_x;
	logic alu_sign_y;
	
	// Flags
	logic carry_flag;
	logic zero_flag;
	logic negative_flag;
	logic overflow_flag;
	wire [3:0] all_flags;
	assign all_flags = {overflow_flag, negative_flag, carry_flag, zero_flag};
	
	// Instruction decoding definitions
	opcode_t opcode;
	wire [funct_width - 1 : 0] funct;
	wire [15:0] immediate_16;
	wire [7:0] immediate_8;
	logic [reg_width - 1:0] write_select;
	logic [reg_width - 1:0] reg_update_select;
	logic [reg_width - 1:0] a_select;
	wire [reg_width - 1:0] b_select;
	wire [reg_width - 1:0] c_select;
	
	always_comb opcode = opcode_t'(instruction[31:27]);
	assign funct = instruction[26:23];
	assign immediate_16 = instruction[15:0];
	assign immediate_8 = instruction[7:0];
	
	// Special case for when we're writing back to the link register during a branch.
	always_comb begin
		if (opcode == kOpcodeBranch) begin
			write_select <= link_register;
		end else begin
			write_select <= instruction[22:18];
		end
	end
	// Special case for a_select needed for upper-half load and when we want to use
	// the usual destination register for a branch
	always_comb begin
		if ((opcode == kOpcodeLoad && funct == kLoadImmediateUpper) || (opcode == kOpcodeBranch)) begin
			a_select <= instruction[22:18];
		end else begin
			a_select <= instruction[17:13];
		end
	end
	assign b_select = instruction[12:8];
	// Used for multiplication.
	assign c_select = instruction[7:3];

	task update_flags();
		zero_flag <= result == 0;
		negative_flag <= result[data_width - 1];
		overflow_flag <= (result[data_width - 1] && !alu_sign_x && !alu_sign_y) ||
							  (!result[data_width - 1] && alu_sign_x && alu_sign_y);
	endtask
	
	// Updates a register at select with value. This uses the second port of the register file.
	task update_register(input logic [reg_width - 1 : 0] select, 
							   input logic [data_width - 1 : 0] value);
		reg_update_enable <= 1;
		reg_update_select <= select;
		reg_update_data <= value;
	endtask
	
	// Set up a 32 register file.
	// TODO(austere): For multiplication results we need to be able to adjust
	// the write select to c_select.
	Regfile #(.data_width(data_width)) regs(
					 .clk(clk), 
					 .a_select(a_select),
					 .b_select(b_select),
					 .write_a_select(write_select),
					 .write_a_enable(reg_write_enable),
					 .write_a_data(result),
					 .write_b_select(reg_update_select),
					 .write_b_enable(reg_update_enable),
					 .write_b_data(reg_update_data),
					 .a(register_a),
					 .b(register_b));
	
	// Determines whether a memory access should occur internally.
	logic memory_access_internal;

	task deassert_signals();
		read_en <= 0;
		write_en <= 0;
		reg_write_enable <= 0;
		reg_update_enable <= 0;
		memory_access_internal <= 0;
		update_flags_enable <= 0;
		alu_sign_x <= 0;
		alu_sign_y <= 0;
		fifo_ready_out <= 0;
		fifo_valid_out <= 0;
	endtask
	
	always @(posedge clk) begin
		if (reset) begin
			// Execution will always start at 0.
			pc <= 0;
			halted <= 0;
			deassert_signals();
			state <= kStateFetch;
		end else begin
			case (state)
				kStateFetch: fetch();
				kStateDecode: decode();
				kStateExecute: execute();
				kStateMemoryAccess: memory_access();
				default: halt();
			endcase
		end
	end
	
	task fetch();
		if (update_flags_enable) begin
			update_flags();
		end
		deassert_signals();
		// IMEM access.
		if (!pause) begin
			instruction <= imem[pc];
			pc <= pc + 1;
			state <= kStateDecode;
		end
	endtask
	
	task decode();
		// Special case
		relative_pc <= pc + $signed(immediate_16);
		relative_pc8 <= pc + $signed(immediate_8);
		if (opcode == kOpcodeFifo) begin
			if (funct == kFifoPush || funct == kFifoPop) begin
				fifo_select <= (1 << instruction[2:0]);
			end
		end
		state <= kStateExecute;
	endtask
		
	task execute();
		// Note: Execute will do the next state transition.
		case (opcode)
			kOpcodeSpecial: execute_special();
			kOpcodeLoad: execute_load();
			kOpcodeStore: execute_store();
			kOpcodeMove: execute_move();
			kOpcodeAlu: execute_alu();
			kOpcodeShift: execute_shift();
			kOpcodeCompare: execute_compare();
			kOpcodeBranch: execute_branch();
			kOpcodeBranchCond: execute_branch_cond();
			kOpcodeFifo: execute_fifo();
			default: halt();
		endcase
	endtask
	
	task memory_access();
		// Note: Memory access will do the next state transition.
		case (opcode)
			kOpcodeLoad: begin
				memory_access_load();
			end
			kOpcodeStore: begin
				memory_access_store();
			end
		endcase
	endtask
	
	task halt();
		deassert_signals();
		halted <= 1;
		state <= kStateHalted;
	endtask

	// =========================================================================
	// Special instructions.
	// =========================================================================
	// Option values.
	
	task execute_special();
		case (funct)
			kSpecialNop: begin
				// Do nothing.
				state <= kStateFetch;
			end
			kSpecialHalt: halt();
			kSpecialClearFlags: begin
				zero_flag <= 0;
				carry_flag <= 0;
				negative_flag <= 0;
				overflow_flag <= 0;
				state <= kStateFetch;
			end
			default: halt();
		endcase
	endtask	
	
	// =========================================================================
	// Load instructions.
	// =========================================================================
	// Load helper function.
	task load_data(input logic [address_width - 1 : 0] address_out);
		address <= address_out;
		if (in_imem(address_out)) begin
			memory_access_internal <= 1;
		end else begin
			memory_access_internal <= 0;
			read_en <= 1;
		end
		state <= kStateMemoryAccess;
	endtask
	
	task execute_load();
		case (funct)
			kLoadImmediate: begin
				result[15:0] <= immediate_16;
				result[31:16] <= '0;
				// Skip memory access
				reg_write_enable <= 1;
				state <= kStateFetch;
			end
			kLoadImmediateUpper: begin
				// For this instruction register_a is loaded with the write destination.
				result[15:0] <= register_a[15:0];
				result[31:16] <= immediate_16;
				// Skip memory access
				reg_write_enable <= 1;
				state <= kStateFetch;
			end
			kLoadRelative: load_data(relative_pc);
			kLoadIndirect: load_data(register_a);
			kLoadDisplacement: load_data(register_a + $signed(immediate_8));
			kLoadIndirectPostIncrement: begin
				load_data(register_a);
				update_register(a_select, register_a + 1);
			end
			kLoadIndirectPreDecrement: begin
				load_data(register_a - 1);
				update_register(a_select, register_a - 1);
			end
			default: halt();
		endcase
	endtask
	
	task complete_memory_access_load();
		reg_write_enable <= 1;
		state <= kStateFetch;
	endtask

	task memory_access_load();
		reg_update_enable <= 0;
		if (memory_access_internal) begin
			result <= imem[address];
			complete_memory_access_load();
		end else if (data_acknowledge) begin
			result <= data_in;
			read_en <= 0;
			complete_memory_access_load();
		end
	endtask
	
	// =========================================================================
	// Store instructions.
	// =========================================================================
	// Store helper function.
	task store_data(input logic [address_width - 1 : 0] address_out, 
	                input logic [data_width - 1 : 0] data);
		address <= address_out;
		if (in_imem(address_out)) begin
			memory_access_internal <= 1;
		end else begin
			memory_access_internal <= 0;
			data_out <= data;
			write_en <= 1;
		end
		state <= kStateMemoryAccess;
	endtask
	
	task execute_store();
		case(funct)
			kStoreRelative: store_data(.address_out(relative_pc8), .data(register_a));
			kStoreIndirect: store_data(.address_out(register_b), .data(register_a));
			kStoreIndirectPostIncrement: begin
				store_data(.address_out(register_b), .data(register_a));
				update_register(b_select, register_b + 1);
			end
			kStoreIndirectPostIncrement: begin
				store_data(.address_out(register_b - 1), .data(register_a));
				update_register(b_select, register_b - 1);
			end
			default: halt();
		endcase
	endtask
	
	task memory_access_store();
		reg_update_enable <= 0;
		if (memory_access_internal) begin
			// Always stored from register_a.
			imem[address] <= register_a;
			state <= kStateFetch;
		end else if (data_acknowledge) begin
			write_en <= 0;
			state <= kStateFetch;
		end
	endtask

	// =========================================================================
	// Move instructions.
	// =========================================================================
	task do_move(logic [data_width - 1 : 0] result_in);
		result <= result_in;
		reg_write_enable <= 1;
		state <= kStateFetch;
	endtask
	
	task execute_move();
		case(funct)
			kMoveDirect: do_move(register_a);
			kMoveNegate: do_move(~register_a);
			kMoveFlags:  do_move(all_flags);
			kMoveZero:	 do_move('0);
			default: halt();
		endcase
	endtask

	// =========================================================================
	// ALU instructions.
	// =========================================================================
	// ALU helper tasks.
	// High bit of result_in will contain a carry flag (potentially).
	// x and y are used to determine the sign of the operators used to calculate
	// the overflow flag.
	task do_alu(logic [data_width : 0] result_in, 
					logic is_subtract = 0, 
					logic [data_width - 1 : 0] x, 
					logic [data_width - 1 : 0] y);
		result <= result_in[data_width - 1 : 0];
		reg_write_enable <= 1;
		update_flags_enable <= 1;
		alu_sign_x <= x[data_width - 1];
		if (is_subtract) begin
			alu_sign_y <= !y[data_width - 1];
			carry_flag <= ~result_in[data_width];
		end else begin
			alu_sign_y <= y[data_width - 1];
			carry_flag <= result_in[data_width];
		end
		state <= kStateFetch;
	endtask
	
	task execute_alu();
		case (funct)
			kAluAdd:	do_alu(.result_in(register_a + register_b),
								 .x(register_a), .y(register_b));
			kAluAdc: do_alu(.result_in(register_a + register_b + carry_flag), 
								 .x(register_a), .y(register_b));
			kAluSub: do_alu(.result_in(register_a + ~register_b + 1), 
								 .is_subtract(1), .x(register_a), .y(register_b));
			kAluSbb: do_alu(.result_in(register_a + ~register_b + !carry_flag),
								 .is_subtract(1), .x(register_a), .y(register_b));
			kAluAnd: do_alu(.result_in(register_a & register_b),
								 .x(register_a), .y(register_b));
			kAluOr:	do_alu(.result_in(register_a | register_b),
								 .x(register_a), .y(register_b));
			kAluXor:	do_alu(.result_in(register_a ^ register_b),
								 .x(register_a), .y(register_b));
			kAluBitc: do_alu(.result_in(register_a & ~register_b),
								 .x(register_a), .y(register_b));
			// Note: y is set to zero because imm16 is not sign extended so can't
			// be negative anyway and do_alu() only uses the sign.
			// TODO(austere): Being limited to immediate8 is a serious limitaiton.
			kAluAddi: do_alu(.result_in(register_a + immediate_8),
								 .x(register_a), .y(0));
			kAluAdci: do_alu(.result_in(register_a + immediate_8 + carry_flag),
								 .x(register_a), .y(0));
			kAluSubi: do_alu(.result_in(register_a + ~immediate_8 + 1),
								 .is_subtract(1), .x(register_a), .y(0));
			kAluSbbi: do_alu(.result_in(register_a + ~immediate_8 + !carry_flag),
								 .is_subtract(1), .x(register_a), .y(0));
			kAluAndi: do_alu(.result_in(register_a & immediate_8),
								 .x(register_a), .y(0));
			kAluOri:	 do_alu(.result_in(register_a | immediate_8),
								 .x(register_a), .y(0));
			kAluXori: do_alu(.result_in(register_a ^ immediate_8),
								 .x(register_a), .y(0));
			kAluBitci: do_alu(.result_in(register_a & ~immediate_8),
								 .x(register_a), .y(0));
			// This should never happen as all values are accounted for.
			default: halt();
		endcase
	endtask
	
	// =========================================================================
	// Shift instructions.
	// =========================================================================
	task shift_left(logic [data_width : 0] result_in);
		{carry_flag, result} <= result_in;
		reg_write_enable <= 1;
		state <= kStateFetch;
	endtask

	task shift_right(logic [data_width : 0] result_in);
		{result, carry_flag} <= result_in;
		reg_write_enable <= 1;
		state <= kStateFetch;
	endtask
	
	task execute_shift();
		case(funct)
			kShiftLeft1: shift_left({register_a, 1'b0});
			kShiftRight1: shift_right({1'b0, register_a});

			kShiftLeft2: shift_left({register_a[data_width - 2 : 0], 2'b0});
			kShiftRight2: shift_right({2'b0, register_a[data_width - 1 : 1]});

			kShiftLeft4: shift_left({register_a[data_width - 4 : 0], 4'b0});
			kShiftRight4: shift_right({4'b0, register_a[data_width - 1 : 3]});

			kShiftLeft8: shift_left({register_a[data_width - 8 : 0], 8'b0});
			kShiftRight8: shift_right({8'b0, register_a[data_width - 1 : 7]});

			kShiftArithRight: shift_right({register_a[data_width-1], register_a});
			kShiftRollLeft: shift_left({register_a, carry_flag});
			kShiftRollRight: shift_right({carry_flag, register_a});
			default: halt();
		endcase
	endtask

	// =========================================================================
	// Compare instructions.
	// =========================================================================
	task do_compare(logic [data_width : 0] result_in,
					logic [data_width - 1 : 0] x,
					logic [data_width - 1 : 0] y);
		result <= result_in[data_width - 1 : 0];
		update_flags_enable <= 1;
		alu_sign_x <= x[data_width - 1];
		alu_sign_y <= !y[data_width - 1];
		carry_flag <= ~result_in[data_width];
		state <= kStateFetch;
	endtask
	
	task execute_compare();
		case(funct)
			kCompareImmediate:
				do_compare(.result_in(register_a + ~$signed(immediate_8) + 1),
							  .x(register_a),
							  .y($signed(immediate_8)));
			kCompareDirect:
				do_compare(.result_in(register_a + ~register_b + 1),
							  .x(register_a),
							  .y(register_b));
			kCompareTestImmediate: begin
				result <= register_a & immediate_8;
				update_flags_enable <= 1;
				state <= kStateFetch;
			end
			kCompareTestDirect: begin
				result <= register_a & register_b;
				update_flags_enable <= 1;
				state <= kStateFetch;
			end
			default: halt();
		endcase
	endtask
	
	// =========================================================================
	// Branch instructions.
	// =========================================================================
	task do_branch(logic [imem_address_width - 1 : 0] next_pc, logic condition, logic link);
		if (condition) begin
			// write_select is special cased to link_register for branch instructions.
			if (link) begin
				result <= pc;
				reg_write_enable <= 1;
			end
			pc <= next_pc;
		end
		state <= kStateFetch;
	endtask
	
	task execute_branch();
		case(funct)
			kBranchRelative: do_branch(.next_pc(relative_pc), .condition(1), .link(0));
			kBranchAbsolute: do_branch(.next_pc(immediate_16), .condition(1), .link(0));
			kBranchIndirect: do_branch(.next_pc(register_a), .condition(1), .link(0));
			kBranchLookup: do_branch(.next_pc(relative_pc8 + register_a), .condition(1), .link(0));
			kBranchRelativeLink: do_branch(.next_pc(relative_pc), .condition(1), .link(1));
			kBranchAbsoluteLink: do_branch(.next_pc(immediate_16), .condition(1), .link(1));
			kBranchIndirectLink: do_branch(.next_pc(register_a), .condition(1), .link(1));
			kBranchLookupLink: do_branch(.next_pc(relative_pc8 + register_a), .condition(1), .link(1));
			default: halt();
		endcase
	endtask
	
	// =========================================================================
	// Branch conditional instructions.
	// =========================================================================
	task execute_branch_cond();
		case(funct)
			kBranchCondZero: do_branch(.next_pc(relative_pc), .condition(zero_flag), .link(0));
			kBranchCondNotZero: do_branch(.next_pc(relative_pc), .condition(!zero_flag), .link(0));
			kBranchCondCarry: do_branch(.next_pc(relative_pc), .condition(carry_flag), .link(0));
			kBranchCondNotCarry: do_branch(.next_pc(relative_pc), .condition(!carry_flag), .link(0));
			
			kBranchCondMinus: do_branch(.next_pc(relative_pc), .condition(negative_flag), .link(0));
			kBranchCondPlus: do_branch(.next_pc(relative_pc), .condition(!negative_flag), .link(0));
			kBranchCondOverflow: do_branch(.next_pc(relative_pc), .condition(overflow_flag), .link(0));
			kBranchCondNotOverflow: do_branch(.next_pc(relative_pc), .condition(!overflow_flag), .link(0));
			// TODO(austere): Check that the two following ones actually work. Taken from ARM's condition codes
			// but ours may invert the carry on subtract so has to be adjusted. (invert carry).
			// Note: The following two are unsigned comparisions.
			kBranchCondHigher: do_branch(.next_pc(relative_pc), .condition(carry_flag && !zero_flag), .link(0));
			kBranchCondLower: do_branch(.next_pc(relative_pc), .condition(!carry_flag || zero_flag), .link(0));
			kBranchCondGreaterEqual: do_branch(.next_pc(relative_pc), .condition(negative_flag == overflow_flag), .link(0));
			kBranchCondLessThan: do_branch(.next_pc(relative_pc), .condition(negative_flag != overflow_flag), .link(0));
			kBranchCondGreaterThan: do_branch(.next_pc(relative_pc), .condition(!zero_flag && (negative_flag == overflow_flag)), .link(0));
			kBranchCondLessEqual: do_branch(.next_pc(relative_pc), .condition(zero_flag || (negative_flag != overflow_flag)), .link(0));
			default: halt();
		endcase
	endtask

	// =========================================================================
	// Execute FIFO instructions.
	// =========================================================================
	task execute_fifo();
		case(funct)
			kFifoPush: begin
				fifo_data_out <= register_a;
				fifo_valid_out <= 1;
				// Wait until we have successfully pushed.
				if (fifo_ready_in & fifo_select) begin
					state <= kStateFetch;
				end
			end
			kFifoPop: begin
				result <= fifo_data_in;
				reg_write_enable <= 1;
				fifo_ready_out <= 1;
				if (fifo_valid_in & fifo_select) begin
					state <= kStateFetch;
				end
			end
			kFifoPeek: begin
				result <= fifo_data_in;
				reg_write_enable <= 1;
				state <= kStateFetch;
			end
			kFifoWaitReadyAll: begin
				// This is equivalent to fifo_ready_in & immediate_8 == immediate_8.
				if (&(fifo_ready_in & immediate_8 | ~immediate_8)) begin
					state <= kStateFetch;
				end
			end
			kFifoWaitValidAll: begin
				if (&(fifo_valid_in & immediate_8 | ~immediate_8)) begin
					state <= kStateFetch;
				end
			end
			kFifoWaitReadyAny: begin
				if (|(fifo_ready_in & immediate_8)) begin
					state <= kStateFetch;
				end
			end
			kFifoWaitValidAny: begin
				if (|(fifo_valid_in & immediate_8)) begin
					state <= kStateFetch;
				end
			end
			kFifoReadReady: begin
				result <= fifo_ready_in;
				reg_write_enable <= 1;
				state <= kStateFetch;
			end
			kFifoReadValid: begin
				result <= fifo_valid_in;
				reg_write_enable <= 1;
				state <= kStateFetch;
			end
			default: halt();
		endcase
	endtask
endmodule
