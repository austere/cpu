onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /cpu_tb/cpu0/clk
add wave -noupdate /cpu_tb/cpu0/reset
add wave -noupdate /cpu_tb/cpu0/halted
add wave -noupdate -divider Decoding
add wave -noupdate -radix hexadecimal /cpu_tb/cpu0/pc
add wave -noupdate /cpu_tb/cpu0/opcode
add wave -noupdate -radix hexadecimal /cpu_tb/cpu0/funct
add wave -noupdate /cpu_tb/cpu0/state
add wave -noupdate -radix hexadecimal /cpu_tb/cpu0/relative_pc
add wave -noupdate -divider {Temp regs}
add wave -noupdate -radix unsigned /cpu_tb/cpu0/a_select
add wave -noupdate -radix unsigned /cpu_tb/cpu0/b_select
add wave -noupdate -radix hexadecimal /cpu_tb/cpu0/register_a
add wave -noupdate -radix hexadecimal /cpu_tb/cpu0/register_b
add wave -noupdate -radix unsigned /cpu_tb/cpu0/write_select
add wave -noupdate -radix hexadecimal /cpu_tb/cpu0/result
add wave -noupdate /cpu_tb/cpu0/reg_write_enable
add wave -noupdate -divider Memory
add wave -noupdate -radix hexadecimal /cpu_tb/cpu0/address
add wave -noupdate -radix hexadecimal {/cpu_tb/cpu0/imem[16]}
add wave -noupdate -radix hexadecimal {/cpu_tb/cpu0/imem[17]}
add wave -noupdate -radix hexadecimal {/cpu_tb/cpu0/imem[18]}
add wave -noupdate -radix hexadecimal {/cpu_tb/cpu0/imem[19]}
add wave -noupdate -divider Registers
add wave -noupdate -radix hexadecimal {/cpu_tb/cpu0/regs/data[0]}
add wave -noupdate -radix hexadecimal {/cpu_tb/cpu0/regs/data[1]}
add wave -noupdate -radix hexadecimal {/cpu_tb/cpu0/regs/data[2]}
add wave -noupdate -divider Flags
add wave -noupdate /cpu_tb/cpu0/update_flags_enable
add wave -noupdate /cpu_tb/cpu0/alu_sign_x
add wave -noupdate /cpu_tb/cpu0/alu_sign_y
add wave -noupdate /cpu_tb/cpu0/carry_flag
add wave -noupdate /cpu_tb/cpu0/zero_flag
add wave -noupdate /cpu_tb/cpu0/negative_flag
add wave -noupdate /cpu_tb/cpu0/overflow_flag
add wave -noupdate -divider {External responder}
add wave -noupdate -radix hexadecimal /cpu_tb/responder0/address
add wave -noupdate -radix hexadecimal /cpu_tb/responder0/data_in
add wave -noupdate -radix hexadecimal /cpu_tb/responder0/data_out
add wave -noupdate /cpu_tb/responder0/data_acknowledge
add wave -noupdate /cpu_tb/responder0/write_en
add wave -noupdate /cpu_tb/responder0/read_en
add wave -noupdate -divider {Reg update}
add wave -noupdate /cpu_tb/cpu0/reg_update_data
add wave -noupdate /cpu_tb/cpu0/reg_update_enable
add wave -noupdate /cpu_tb/cpu0/reg_update_select
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10819710 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 228
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {10739840 ps} {11015510 ps}
